import {useState, useEffect} from 'react';
import './App.css';
import Navbar from './components/AppNavbar';
import AdminNavbar from './components/AdminNavbar';
import Landing from './pages/Home';
import Footer from './components/Footer';
import Catalog from './pages/Catalog';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminLogin from './pages/AdminLogin';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './pages/CreateProduct';
import {UserProvider} from './UserContext';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

function App() {

  // const [isUserAdmin, setUserAdmin] = useState(false);

  // useEffect(() => {
  //   let isAdmin = localStorage.getItem('accessToken')

  //   if (isAdmin !== undefined && isAdmin !== null) {
  //     setUserAdmin(true);
  //   } else {
  //     setUserAdmin(false);
  //   }
  // }, [localStorage])

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {

    fetch('https://sleepy-fortress-95333.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        }).then(result => result.json()).then(convertedResult => {
            console.log(convertedResult);
            if (convertedResult._id !== "") {
              setUser({
                id: convertedResult._id,
                isAdmin: convertedResult.isAdmin
              })
            } else {
              setUser({
                id: null,
                isAdmin: null
              })
            }
        })
  }, []);
 
  return (

    <UserProvider value={{user, unsetUser, setUser}}>

    <Router>
      <Switch>
        <Route exact path="/" component={Landing}>
        <Navbar />
        <Landing />
        </Route>
        <Route exact path="/products" component={Catalog}>
        <Navbar />
        <Catalog />
        </Route>
        <Route exact path="/login" component={Login}>
        <Navbar />
        <Login />
        </Route>
        <Route exact path="/register" component={Register}>
        <Navbar />
        <Register />
        </Route>
        <Route exact path="/logout" component={Logout}>
        <Navbar />
        <Logout />
        </Route>
        <Route exact path="/products/:id" component={ProductView}>
        <Navbar />
        <ProductView />
        </Route>
        <Route exact path="/admin/login" component={AdminLogin}>
        <AdminNavbar />
        <AdminLogin />
        </Route>
        <Route exact path="/admin/dashboard" component={AdminDashboard}>
        <AdminNavbar />
        <AdminDashboard />
        </Route>
        <Route exact path="/admin/create" component={CreateProduct}>
        <AdminNavbar />
        <CreateProduct />
        </Route>
        <Route component={Error} />
      </Switch>
      <Footer />
    </Router>

    </UserProvider>
  );
}

export default App;
