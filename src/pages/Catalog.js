import {useState, useEffect} from 'react';
import Hero from '../components/Banner';
import Product from '../components/ProductCard';

const bannerInfo = {
    title: "Here are our cookies!",
    tagline: "Have yourself a treat!"
}

export default function Products() {

    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://sleepy-fortress-95333.herokuapp.com/products/available').then(response => response.json()).then(convertedData => {
            setProducts(convertedData.map(subject => {
                return(
                    <Product key={subject._id} productInfo={subject}/>
                );
            }));
        });
    }, []);

    return(
        <>
            <Hero kahitAno={bannerInfo} />
            {products}
        </>
    );
}