import { Container, Card, Row, Col } from 'react-bootstrap';
import {useParams} from 'react-router';
import {useEffect, useState} from 'react';

export default function ProductView() {
	
	const params = useParams();
	console.log(params.id);
	const [product, setProduct] = useState([]);

	useEffect(() => {
		fetch(`https://sleepy-fortress-95333.herokuapp.com/products/${params.id}`).then(res => res.json()).then(convertedData => {
			setProduct(convertedData);
		})
	}, [params])

    return(
        <Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{product.name}</Card.Title>
							<Card.Subtitle>Description:{product.description}</Card.Subtitle>
							<Card.Subtitle>Price: PHP {product.price}</Card.Subtitle>
				
							{/* <Card.Text>8 am - 5 pm</Card.Text>
						
							<Button variant="primary" block>Add to Cart</Button>
			
							<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link> */}
						
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
    )
}