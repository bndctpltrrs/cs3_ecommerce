import React, {useContext, useEffect, useState} from 'react';
import UserContext from '../UserContext';
import {Redirect} from 'react-router';
import {Link} from 'react-router-dom';
import {Row, Button, Container, Col} from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import Hero from '../components/Banner';

const display = {
    title: "Admin Dashboard",
    tagline: "Product Management"
}

export default function AdminDashboard() {
    
    const {user} = useContext(UserContext);
    const [products, setProducts] = useState([]);
    // const [productId, setProductId] = useState('');
    // const [name, setName] = useState('');
    // const [description, setDescription] = useState('');
    // const [price, setPrice] = useState('');
    // const [status, setStatus] = useState('');

    useEffect(() => {
        getItems();
    }, [])

    function getItems() {
        fetch('https://sleepy-fortress-95333.herokuapp.com/products/all').then((result) => {
            result.json().then((res) => {
                setProducts(res);
                // setProductId(res[0]._id);
                // setName(res[0].name);
                // setDescription(res[0].description);
                // setPrice(res[0].price);
                // setStatus(res[0].onStock);
            })
        })
    }
    
    return(
        (user.isAdmin === undefined || user.isAdmin === false) ?

            <Redirect to="/admin/login" />

            :
        
            <Container>  
                <Row>
                    <Col>
                        <Hero kahitAno={display} />
                    </Col>
                </Row>
                
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        products.map((item, i) =>

                        <tr key={i}>
                            <td>{item._id}</td>
                            <td>{item.name}</td>
                            <td>{item.description}</td>
                            <td>{item.price}</td>
                            <td>{item.onStock.toString()}</td>
                        </tr>
                        )
                    }
                    </tbody>
                </Table>

                <Row className="text-right mb-3 mt-5">
                    <Col>
                        <Link to="/admin/create">
                            <Button variant="success" className="btn btn-block">Add New Product</Button>
                        </Link>
                    </Col>
                    
                </Row>
            </Container>
    );
}