import Hero from '../components/Banner';
import Showcase from '../components/Highlights';
import Container from 'react-bootstrap/Container';

let info = {
    title: "You Are at the Home Page",
    tagline: "Chances for Everyone, Everywhere",
    label: "Enroll Now!"
}

export default function Home() {
    return(
        <Container>
            <Hero kahitAno={info} />
            <Showcase />
        </Container>
    );
}