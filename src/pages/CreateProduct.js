import {useState} from 'react';
import Swal from "sweetalert2";
import {useHistory} from "react-router";
import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext.js'
import {useContext} from "react";
import {Redirect} from "react-router";
import Hero from '../components/Banner';

const display = {
    title: "Create a New Product",
    tagline: "Kindly fill up the details below."
}

export default function CreateProduct(event) {

    const history = useHistory();
    const {user} = useContext(UserContext);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    let token = localStorage.getItem('accessToken');

    function createProduct(event) {
        event.preventDefault();

        fetch('https://sleepy-fortress-95333.herokuapp.com/products/create', {
            method: "POST", 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                "name": name,
                "description": description,
                "price": price
            })
        }).then(res => res.json()).then(data => {
            console.log(data);
            if (data === true) {
                Swal.fire({
                    title: 'Product created successfully!',
                    icon: 'success'
                })
                history.push('/admin/dashboard');
            } else {
                Swal.fire({
                    title: "Adding of product failed :(",
                    icon: "danger"
                })
            }
        })
    }

    return(
        (user.isAdmin === undefined || user.isAdmin === false) ?

        <Redirect to="/admin/login" />

        :

        <Container>
            <Row className="mb-3">
                <Col>
                    <Hero kahitAno={display} />
                </Col>
            </Row>

            <Form onSubmit={(event) => createProduct(event)} className="mb-5">
                <Form.Group controlId="name">
                    <Form.Label>Product Name:</Form.Label>
                    <Form.Control type="text" placeholder="Insert the Product Name here" value={name} onChange={event => setName(event.target.value)} required />
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Label>Product Description:</Form.Label>
                    <Form.Control as="textarea" rows={5} placeholder="Insert the Product Description here" value={description} onChange={event => setDescription(event.target.value)} required />
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Product Price:</Form.Label>
                    <Form.Control type="text" placeholder="Insert the Product Price here" value={price} onChange={event => setPrice(event.target.value)} required />
                </Form.Group>

                <Button variant="success" className="btn btn-block" type="submit" id="createBtn">Create Product</Button>

            </Form>
                
        </Container>
    );

}