import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Redirect} from 'react-router-dom';
import {Form, Button, Container} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';

const display = {
    title: "Let's Start by Logging In!",
    tagline: "Let's go! :)"
}

export default function Login() {

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const authenticate = (e) => {
		e.preventDefault()

		fetch(`https://sleepy-fortress-95333.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(information => {
			// console.log(data)
			// localStorage - is used contains information from our web pages (keeps the data forever)
			// setItem - sets the data to our localStorage
			// getItem - get the data from our localStorage
            
            if (typeof information.accessToken !== "undefined") {
                localStorage.setItem('accessToken', information.accessToken);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to the App!"
                });
                retrieveUserDetails(information.accessToken);
            } else {
                Swal.fire({
                    title: "Login Failed",
                    icon: "danger",
                    text: "Check your login details and try again!"
                });
            }
		})
	}

    const retrieveUserDetails = (token) => {
        fetch('https://sleepy-fortress-95333.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(result => result.json()).then(convertedResult => {
            console.log(convertedResult);

            setUser({
                id: convertedResult._id,
                isAdmin: convertedResult.isAdmin
            });
        })
    }

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);


    return(

        (user.id) ?

        <Redirect to="/products" />

        :

        <Container>
            <Hero kahitAno={display} />

            <Form className="mb-5" onSubmit={e => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address: </Form.Label>
                    <Form.Control type="email" placeholder="Insert Email Here" value={email} onChange={e => setEmail(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="userPassword">
                    <Form.Label>Password: </Form.Label>
                    <Form.Control type="password" placeholder="Insert Password Here" value={password} onChange={e => setPassword(e.target.value)} required />
                </Form.Group>
                {
                    isActive ? 

                    <Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
                    :
                    <Button type="submit" id="submitBtn" variant="danger" className="btn btn-block" disabled>Login</Button>
                }
            </Form>
        </Container>
    );
}