import UserContext from '../UserContext';
// import {useHistory} from 'react-router';
import {useState, useContext} from 'react';
import {Redirect} from 'react-router-dom';
import {Form, Button, Container} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';

const display = {
    title: "Admin Login",
    tagline: "Start by logging in."
}

export default function AdminLogin() {

    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // const [isActive, setIsActive] = useState(false);
    // const history = useHistory();

    const authenticate = (e) => {
		e.preventDefault()

        const data = {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		};

		fetch(`https://sleepy-fortress-95333.herokuapp.com/users/login`, data)
		.then(res => {
            if (res.ok) {
                return res.json();
            } else {
                Swal.fire({
                    title: "Login Failed",
                    icon: "warning",
                    text: "Check your login details and try again!"
                });
                throw new Error('Error Occured')
            }
        })
		.then(information => {
			// console.log(data)
			// localStorage - is used contains information from our web pages (keeps the data forever)
			// setItem - sets the data to our localStorage
			// getItem - get the data from our localStorage


            // console.log(information.isAdmin)
            // localStorage.setItem('accessToken', information.accessToken);
            // window.location.reload();
            
            if (information.isAdmin === true) {
                localStorage.setItem('accessToken', information.accessToken);
                retrieveUserDetails(information.accessToken);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to the Admin Dashboard!"
                });
            } else {
                Swal.fire({
                    title: "Login Failed",
                    icon: "warning",
                    text: "Check your login details and try again!"
                });
                // alert('error');
            }
		})
	}

    const retrieveUserDetails = (token) => {

        // const token = localStorage.getItem('accessToken');

        fetch('https://sleepy-fortress-95333.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(result => result.json()).then(convertedResult => {
            console.log(convertedResult);

            setUser({
                id: convertedResult._id,
                isAdmin: convertedResult.isAdmin
            });
        })
    }

	// useEffect(()=>{
	// 	if(email !== "" && password !== ""){
	// 		setIsActive(true)
	// 	} else {
	// 		setIsActive(false)
	// 	}
	// }, [email, password]);


    return(

        (user.isAdmin === true) ?

        <Redirect to="/admin/dashboard" />

        :

        <Container>
            <Hero kahitAno={display} />

            <Form className="mb-5" onSubmit={e => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address: </Form.Label>
                    <Form.Control type="email" placeholder="Insert Email Here" value={email} onChange={e => setEmail(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="userPassword">
                    <Form.Label>Password: </Form.Label>
                    <Form.Control type="password" placeholder="Insert Password Here" value={password} onChange={e => setPassword(e.target.value)} required />
                </Form.Group>
                <Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
                {/* {
                    isActive ? 

                    <Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
                    :
                    <Button type="submit" id="submitBtn" variant="danger" className="btn btn-block" disabled>Login</Button>
                } */}
            </Form>
        </Container>
    );
}