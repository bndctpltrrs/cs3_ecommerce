import {useState, useEffect} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import {useHistory} from 'react-router-dom';

const paraSaBanner = {
    title: "Create Your Own Account Here!",
    tagline: "This is the first step."
}


export default function Register() {

    const history = useHistory();
    const [firstName, setFirstName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2,setPassword2] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isRegBtnActive, setRegBtnActive] = useState('');
    const [isComplete, setIsComplete] = useState(false);
    const [isPwSame, setIsPwSame] = useState(false);

    function registerUser(event) {
        event.preventDefault();
        
        fetch('https://sleepy-fortress-95333.herokuapp.com/users/register', {method: "POST", headers: {
            'Content-Type': 'application/json'
        },

        body: JSON.stringify({
            firstName: firstName,
            middleName: middleName,
            lastName: lastName,
            email: email,
            password: password1,
            mobileNumber: mobileNo
        })
    
    }).then(res => res.json).then(data => {
        Swal.fire({
            title: `Your account is registered successfully!`,
            icon: `success`,
            text: 'Try logging in to our page.'
        })
        history.push('/login');
    })

    }


    useEffect(() => {
        if ((firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '') && (password1 === password2)) {
            setRegBtnActive(true);
            setIsComplete(true);
            setIsPwSame(true);
          } else {
            setRegBtnActive(false);
            setIsComplete(false);
            setIsPwSame(false);
          }
        },[firstName, middleName, lastName, email, password1, password2, mobileNo]);      

    return(
        <Container>
            <Hero kahitAno={paraSaBanner} />

            {
                isComplete ? 
                <h4 className="mb-5">Proceed with Register</h4>
                :
                <h4 className="mb-5">Fill up the form below</h4>
            }

            <Form onSubmit={(event) => registerUser(event)} className="mb-5">
                <Form.Group controlId="firstName">
                    <Form.Label>First Name:</Form.Label>
                    <Form.Control type="text" placeholder="Insert First Name Here" value={firstName} onChange={event => setFirstName(event.target.value)} required />
                </Form.Group>

                <Form.Group controlId="middleName">
                    <Form.Label>Middle Name:</Form.Label>
                    <Form.Control type="text" placeholder="Insert Middle Name Here" value={middleName} onChange={event => setMiddleName(event.target.value)} required />
                </Form.Group>

                <Form.Group controlId="lastName">
                    <Form.Label>Last Name:</Form.Label>
                    <Form.Control type="text" placeholder="Insert Last Name Here" value={lastName} onChange={event => setLastName(event.target.value)} required />
                </Form.Group>

                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address: </Form.Label>
                    <Form.Control type="email" placeholder="Insert Email Here" value={email} onChange={event => setEmail(event.target.value)} required />
                </Form.Group>

                <Form.Group controlId="password1">
                    <Form.Label>Password: </Form.Label>
                    <Form.Control type="password" placeholder="Insert Password Here" value={password1} onChange={event => setPassword1(event.target.value)} required />
                </Form.Group>

                {
                    isPwSame ?
                    <p className="text-success">*Password matched!*</p>
                    :
                    <p className="text-danger">*Password didn't match.*</p>
                }

                <Form.Group controlId="password2">
                    <Form.Label>Confirm Password: </Form.Label>
                    <Form.Control type="password" placeholder="Confirm Password Here" value={password2} onChange={event => setPassword2(event.target.value)} required />
                </Form.Group>

                {/* <Form.Group controlId="gender">
                    <Form.Label>Select Gender:</Form.Label>
                    <Form.Control as="select" value={gender} onChange={event => {setGender(event.currentTarget.value)}}>
                        <option selected disabled>Select Gender Here</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </Form.Control>
                </Form.Group> */}

                <Form.Group controlId="mobileNo">
                    <Form.Label>Mobile Number: </Form.Label>
                    <Form.Control type="number" placeholder="Insert Mobile Number Here" value={mobileNo} onChange={event => setMobileNo(event.target.value)} 
                    onInput = {(e) => {
                        e.target.value = e.target.value.slice(0,11)
                        }}
                    required />
                </Form.Group>

                {isRegBtnActive ? 
                    <Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">Create New Account</Button>
                    : 
                    <Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>Create New Account</Button>
                }

            </Form>
        </Container>
    );
}