import Hero from '../components/Banner';
import {Container} from 'react-bootstrap';

const forBanner = {
    title: "Page Not Found",
    tagline: "Seems like we cannot find the page :("
}

export default function Error() {
    return(
        <Container>
            <Hero kahitAno={forBanner} />
        </Container>
    );
}