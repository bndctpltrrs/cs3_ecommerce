import {Row, Col, Image} from 'react-bootstrap';
import bannerImage from '../images/banner.jpg';

function Banner({kahitAno}) {
  // const {title, tagline} = kahitAno;

  return(
    <Row>
      <Col>
        <Image src={bannerImage} fluid />
      </Col>
      {/* <Col className="p-5">
        <h1>{title}</h1>
        <p>{tagline}</p>
      </Col> */}
    </Row>
    )
  }
     
export default Banner;
     