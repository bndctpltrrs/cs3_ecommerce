import {useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
function AppNavbar() {

   const {user} = useContext(UserContext);

	return(

		<Navbar bg="light" expand="lg">
			<Navbar.Brand >
			   Slyng Admin
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav"> 

               {
                  (user.id) ?
                  <>
                    <Nav.Link as={NavLink} to="/admin/dashboard">Dashboard</Nav.Link>
                    <Nav.Link as={NavLink} to="/admin/create">Add Product</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
                  :
                  <>
                     
                  </>
               }
            </Navbar.Collapse>
		</Navbar>
	)
}

export default AppNavbar;