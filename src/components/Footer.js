import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

export default function Footer () {
    return (
        <Navbar className="mt-5" fixed="bottom" bg="warning" variant="light">
            <Container>
                <Navbar.Text>Slyng Cookies</Navbar.Text>
                {/* <Navbar.Text>Zuitt Coding Bootcamp</Navbar.Text> */}
                <Navbar.Text>&copy; 2021, Benedict Paulo Torres</Navbar.Text>
            </Container>
        </Navbar>
    );
}