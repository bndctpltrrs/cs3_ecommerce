import Card from 'react-bootstrap/Card';
import {Link} from 'react-router-dom';

export default function ProductCard({productInfo}) {

    const {_id, name, description, price} = productInfo;
    return(
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
                <Link to={`/products/${_id}`}>
                    <button className="btn btn-warning" type="button">See Details</button>
                </Link>
            </Card.Body>
        </Card>
    );
}