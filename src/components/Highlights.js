import {Row, Col, Card} from 'react-bootstrap';
import ghostCookie from '../images/ghost.jpg';
import genesisCookie from '../images/genesis.jpg';
import mannaCookie from '../images/manna.jpg';

export default function Highlights() {
    return(
        
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Img variant="top" src={ghostCookie} />
                    <Card.Body>
                        <Card.Title>
                            GHOST
                        </Card.Title>
                        <Card.Text>
                            Our best-seller! Cookie filled with cream cheese.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Img variant="top" src={genesisCookie} />
                    <Card.Body>
                        <Card.Title>
                            GENESIS
                        </Card.Title>
                        <Card.Text>
                            Our cookie full of chocolate per bite!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Img variant="top" src={mannaCookie} />
                    <Card.Body>
                        <Card.Title>
                            MANNA
                        </Card.Title>
                        <Card.Text>
                            Fan of cookie and cream? Try our MANNA cookies!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}