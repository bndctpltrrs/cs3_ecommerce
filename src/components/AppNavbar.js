import {useContext} from 'react';
import {Navbar, Nav, Image} from 'react-bootstrap';
// import Nav from 'react-bootstrap/Nav';
import {NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import slyngLogo from '../images/logo.jpg';
function AppNavbar() {

   const {user} = useContext(UserContext);

   const clearStorage = () => {
      window.localStorage.removeItem("accessToken");
      window.location.reload();
    }

	return(

		<Navbar bg="light" expand="lg">
			<Navbar.Brand>
            <Image src={slyngLogo} width="50" height="50" />
         </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav"> 
               <Nav.Link as={NavLink} to="/">Home</Nav.Link>
               <Nav.Link as={NavLink} to="/products">Cookies</Nav.Link>
               {/* <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link> */}
               {
                  
                  (user.id) ?
                  
                  <Nav.Link href="/login" className="links ml-auto" onClick={clearStorage}>Logout</Nav.Link>
                  :
                  <>
                     <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                     <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  </>
               }
            </Navbar.Collapse>
		</Navbar>
	)
}

export default AppNavbar;